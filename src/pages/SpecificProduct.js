import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from "../UserContext";
import Swal from 'sweetalert2'
export default function ProductView(){
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [price, setPrice] = useState(0);
	const {user} = useContext(UserContext);
	const {productId} = useParams();


	const history = useNavigate();
	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URI}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setName(data.title);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	const addToCart = (productId) => {
		fetch(`${process.env.REACT_APP_API_URI}/cart/addtocart/${productId}`, {
			method: "POST",
			headers: {
				"Content-Type": 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body : JSON.stringify({
        	quantity: Number(quantity)
        })
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
			if(data){
				Swal.fire({
					title: "Added to cart!",
					icon: "success",
					text: "Thank you!"
				})

				history("/products");
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please Try Again!"
				})
				history("/");
			}
		})
	}

	return(
		<Container>
			<Row>
			<Col lg = {{span: 6, offset: 3}} className = "mt-3">
				<Card>
					    <Card.Body className="text-center p-3">
									<Card.Title className="text-center mt-2 mb-2">{name}</Card.Title>
									<Card.Subtitle className="text-center mt-2 mb-2">Description:</Card.Subtitle>
									<Card.Text className="text-center mt-2 mb-2">{description}</Card.Text>
									<Card.Subtitle className="text-center mt-2 mb-2">Quantity:</Card.Subtitle>
									<input value = {quantity} className="text-center mt-2 mb-2" onChange = {e => setQuantity(e.target.value)} type = "number"/>
									<Card.Subtitle className="text-center mt-2 mb-2">Price:</Card.Subtitle>
									<Card.Text className="text-center mt-2 mb-2">PhP {price}</Card.Text>
									{user.id !== null && user.isAdmin && 
										<Button as = {Link} to = {`/adminUpdate/${productId}`} variant="outline-success">Edit</Button>  
										 }
									{user.id !== null && !user.isAdmin && <Button variant="outline-success" onClick = {()=> addToCart(productId)}>Add to Cart</Button> }
									{user.id === null && <Button as = {Link} to = "/login" variant="outline-success">Add to Cart</Button> }
								</Card.Body>	
					</Card>
			</Col>
			</Row>
		</Container>
		)
}