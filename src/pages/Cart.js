import {Fragment, useEffect, useState} from 'react';
import CartItem from '../components/CartItem';

export default function Cart(){

	const [userCart, setUserCart] = useState([]);

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URI}/cart`, {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },     
    })
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setUserCart(data.map(cartItem => {

		return(
			<CartItem key = {cartItem._id} cartItem = {cartItem}/>)
		}))
	})

	}, []);

	return(
		<Fragment>
		{userCart}
		</Fragment>
		)
}