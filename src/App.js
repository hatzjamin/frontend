import './App.css';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import {useState, useEffect} from 'react';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import AllProducts from './pages/AllProducts';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout  from './pages/Logout';
import NotFound from './pages/NotFound';
import SpecificProduct from './pages/SpecificProduct'
import AddProduct from './pages/AddProduct';
import AdminDashboard from './pages/AdminDashboard';
import Cart from "./pages/Cart"
import {UserProvider} from './UserContext';

import React from 'react';
import Footer from './components/Footer';
import UpdateProduct from './pages/UpdateProduct';

function App() {

  // const name = 'John Smith';
  // const element = <h1>Hello, {name}</h1>

  // State hook for the user that will be globally accessible using the useContext
  // This will also be used to store the user information and be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({id: null, isAdmin: false});

  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(()=> {
    console.log(user)
  }, [user]);

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URI}/users/profile`,
        {headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }})
        .then(response => response.json())
        .then(data => {
          console.log(data);
          setUser({id: data._id, isAdmin: data.isAdmin})
      })
}, [])

  return (
    // Router/BrowserRouter > Router > Route
  <UserProvider value = {{user, setUser, unSetUser}}>
    <Router> 
      <AppNavbar/>
      <Routes>
          <Route path = '*' element={<NotFound/>}/>
          <Route path = "/" element = {<Home/>}/>
          <Route path = "/products" element = {<Products/>}/>
          <Route path = "/allProducts" element = {<AllProducts/>}/>
          <Route path = "/login" element = {<Login/>}/>
          <Route path = "/register" element = {<Register/>}/>
          <Route path = "/logout" element = {<Logout/>}/>
          <Route path= "/add" element={<AddProduct/>} />
          <Route path='/dashboard' element={<AdminDashboard />} />
          <Route path='/cart' element={<Cart />} />
          <Route path = "/products/:productId" element = {<SpecificProduct/>}/>
          <Route path = "/adminUpdate/:productId" element = {<UpdateProduct/>}/>
      </Routes> 

    </Router>
  </UserProvider>
  );
}

export default App;
