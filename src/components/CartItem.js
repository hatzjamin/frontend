import {useState, useEffect} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap'
import {useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2'
export default function CartItem({cartItem}){
const history = useNavigate();
function checkout(){
	fetch(`${process.env.REACT_APP_API_URI}/cart/checkout/${cartItem._id}`, {
		method: 'POST',
		headers: {'Content-Type': 'application/json',
		Authorization: `Bearer ${localStorage.getItem('token')}`
		}
	}).then(response => response.json())
	.then(data => {
		console.log(data);
		if(data){
				Swal.fire({
					title: "Successfully bought!",
					icon: "success",
					text: "Thank you!"
				})

				history("/products");
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please Try Again!"
				})
				// history("/");
			}
	})
}

	return(
		<Container>
			<Row>
			<Col lg = {{span: 6, offset: 3}} className = "mt-2">
				<Card className = "mt-3 mb-3">
					    <Card.Body className="text-center">
									<Card.Title>{cartItem.title}</Card.Title>
									<Card.Subtitle>Quantity:</Card.Subtitle>
									<Card.Text>{cartItem.quantity}</Card.Text>
									<Card.Subtitle>Amount:</Card.Subtitle>
									<Card.Text>PhP {cartItem.amount}</Card.Text>
									<Button variant="outline-success" onClick = {checkout}>Checkout</Button>
								</Card.Body>	
					</Card>
			</Col>
			</Row>
		</Container>
		)
}