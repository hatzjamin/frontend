import {useState, useEffect, useContext} from 'react';
import {Row, Col, Card} from 'react-bootstrap'
import Button from "react-bootstrap/Button";
import UserContext from '../UserContext';
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}){

	const {_id, title, description, price, stock} = productProp;

	const [customer, setCustomer] = useState(0);
	const [stocksAvailble, setStocksAvailable] = useState(stock);

	const [isAvailable, setIsAvailable] = useState(true);

	const {user} = useContext(UserContext);

	// Add an "useEffect" hook to have "CourseCard" component do perform a certain task every DOM update
		// Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

	useEffect(()=>{
		if(stocksAvailble === 0){

		setIsAvailable(false);

		}
	 	console.log(isAvailable);

	}, [stocksAvailble]);

	function buy(){
		if(stocksAvailble === 1){
			alert("Congratulations, you were able to before the cut!");
		}
		setCustomer(customer+1)
		setStocksAvailable(stocksAvailble-1);
	}


	return(
		<Row>
		<Col xs = {12} md = {4} className = "offset-md-4 offset-0 mt-3 mb-3">
		<Card className = "h-100 p-3">
		      <Card.Body>
		        <Card.Title>{title}</Card.Title>
		        <Card.Text className="h6">Description:</Card.Text>
		        <Card.Text>
		          {description}
		        </Card.Text>
		        <Card.Text className = "h6">Price:</Card.Text>
		        <Card.Text>
		          P{price}
		        </Card.Text>
		        <Card.Text className = "h6">Stocks available:</Card.Text>
		        <Card.Text>
		          {stock}
		        </Card.Text>
		       {
		       	(user !== null) ?
		       	 <Button variant="outline-success" as = {Link} to = {`/products/${_id}`} disabled = {!isAvailable}>Details</Button>
		       	 :
		       	  <Button as = {Link} to = "/login" variant="outline-success"  disabled = {!isAvailable}>Details</Button>
		       }
		      </Card.Body>
		    </Card>
		</Col>
		</Row>
		)
}