import Button from "react-bootstrap/Button";
/* Bootstrap Grid System*/
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import {Link} from "react-router-dom"


export default function Banner(){

	return(
		<Row className = "justify-content-center">
			<Col className = "text-center">
				<h1>Bocchitars</h1>
				<p>The best quality of guitars out there!</p>

				<Button as = {Link} to = "/products" variant = "outline-info">Check our products!</Button>
			</Col>
		</Row>
		)
}